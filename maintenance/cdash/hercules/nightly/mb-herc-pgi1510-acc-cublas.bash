bname=$(basename "$0")
bname=${bname/.bash/}
##########
export LM_LICENSE_FILE=/opt/pgi/license.dat
source /opt/pgi/linux86-64/15.10/pgi.sh
export OMP_NUM_THREADS=6
export CUDA_HOME=/usr/local/cuda-7.5
export LD_LIBRARY_PATH=${CUDA_HOME}/lib64
export PATH=${CUDA_HOME}/bin:${PATH}
export DALTON_NUM_MPI_PROCS=1
export LSDALTON_DEVELOPER=1
export CUDA_VISIBLE_DEVICES=0,1 # only use k40 cards
#
wrk=$1
lib=lsdalton_$bname
export DALTON_TMPDIR=$wrk/$lib/$bname/tmp
#
if [ ! -d $wrk/$lib ]
then
   cd $wrk
   git clone --recursive git@gitlab.com:dalton/lsdalton.git $lib
fi
#
cd $wrk/$lib
#
if [ ! -d $wrk/$lib/$bname ]
then
   ./setup --fc=pgf90 --cc=pgcc --cxx=pgcpp --omp --gpu --cublas --extra-fc-flags="-ta=host,tesla:cc35 -lcuda -Mcuda=7.5 -lcublas" -DENABLE_EFS=OFF -DENABLE_VPOTDAMP=OFF -DENABLE_XCFUN=OFF -DENABLE_RSP=OFF -DENABLE_PCMSOLVER=OFF -DENABLE_DEC=ON -DENABLE_TENSORS=ON -DBUILDNAME="$bname" $bname
fi
if [ ! -d $DALTON_TMPDIR ]
then
   mkdir $DALTON_TMPDIR
fi
#
cd $bname
#
ctest -D Nightly -L linsca
