=========
ChangeLog
=========

Unreleased
----------

Added
+++++

- General-order electric and geometrical derivatives through a combination 
  of the OpenRSP library and HODI integral routines (only HF)
- Single residues of electric dipole response properties for calculation 
  of MPA strengths, currently tested for 2PA and 3PA
- General-order electrostatic-potential integral-derivative components
- Tests on PCM static and dynamic first hyperpolarizability
- Tests on PCM Two Photon Absorption
- Tests on PCM static and dynamic second hyperpolarizability (only HF)
- Polarizable embedding through FraME library

Changed
+++++++

- Docker images for CI. The image now used is based on Ubuntu 16.04 and
  includes GCC 5.3.1, OpenMPI 1.10 and MKL 2017.4.239.
  The Dockerfile can be found `here <https://github.com/robertodr/dockerfiles/blob/master/Dockerfile.ubuntu-16.04_gcc5.3.1_openmpi1.10_mkl2017.4.239>`_.

Fixed
+++++

- ``setup`` script, test script, compilations framework, and more, compatible with Python 3.

1.2 (2016-02-08)
----------------

- A bug in using cartesian basis functions was identified and fixed.

1.1 (2016-01-13)
----------------

- The "make install" was fixed.

1.0 (2015-12-22)
----------------

See the release notes at http://daltonprogram.org for a list of new features in
Dalton and LSDalton.
