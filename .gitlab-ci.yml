variables:
  GIT_SSL_NO_VERIFY: "true"
  GIT_STRATEGY: "clone"
  GIT_SUBMODULE_STRATEGY: "recursive"
  LSDALTON_DEVELOPER: "1"
  DALTON_TMPDIR: "/tmp/${GITLAB_USER_ID}_${CI_COMMIT_SHA}"
  BUILD_COMMAND: "cmake --build . -- --jobs=2 VERBOSE=1"
  CTEST_COMMAND: "ctest --output-on-failure --verbose -L ContinuousIntegration"

mpi-gcc-int64:
  image: devcafe/ubuntu16.04-gcc5.3.1-openmpi1.10-mkl2017.4.239:latest
  variables:
    BUILD_NAME: "build-mpi-gcc-int64-mkl_user=${GITLAB_USER_ID}_branch=${CI_COMMIT_REF_NAME}"
    DALTON_NUM_MPI_PROCS: "3"
    OMP_NUM_THREADS: "1"
  before_script:
    - whoami
    - uname -a
    - free -m
    - df -h
    - ulimit -a
    - ulimit -s unlimited
    - lscpu | egrep 'Thread|Core|Socket|^CPU\('
    - virtualenv venv
    - source venv/bin/activate
    - pip install numpy matplotlib docopt
    - cmake --version
    - python -V
    - mpif90 --version
    - mpicc --version
    - mpic++ --version
    - echo BUILD_NAME=${BUILD_NAME}
    - echo DALTON_NUM_MPI_PROCS=${DALTON_NUM_MPI_PROCS}
    - echo OMP_NUM_THREADS=${OMP_NUM_THREADS}
    - git submodule sync --recursive
    - git submodule update --init --recursive
  script:
    - mkdir -p ${DALTON_TMPDIR}
    - >
        python setup --fc=mpif90 --cc=mpicc --cxx=mpic++ --mpi --type=debug --check --int64
        -DENABLE_DEC=ON
        -DENABLE_TENSORS=ON
        -DTENSORS_ENABLE_BRUTE_FORCE_FLUSH=ON
        -DENABLE_RSP=ON
        -DENABLE_XCFUN=ON
        -DENABLE_PCMSOLVER=ON
        -DBUILDNAME="${BUILD_NAME}" ${BUILD_NAME}
    - cd ${BUILD_NAME}
    - echo Build step executing ${BUILD_COMMAND}
    - bash ${CI_PROJECT_DIR}/.scripts/ci_build.sh
    - echo Test step executing ${CTEST_COMMAND}
    - python ${CI_PROJECT_DIR}/.scripts/ci_test.py ${CTEST_COMMAND}
    - python ${CI_PROJECT_DIR}/.scripts/ci_print_failing.py
  artifacts:
    paths:
      - ${BUILD_NAME}/build.log
      - ${BUILD_NAME}/full_ctest_output.dat

openmp-gcc:
  image: devcafe/ubuntu16.04-gcc5.3.1-openmpi1.10-mkl2017.4.239:latest
  variables:
    BUILD_NAME: "build-openmp-gcc-mkl_user=${GITLAB_USER_ID}_branch=${CI_COMMIT_REF_NAME}"
    DALTON_NUM_MPI_PROCS: "1"
    OMP_NUM_THREADS: "6"
  before_script:
    - whoami
    - uname -a
    - free -m
    - df -h
    - ulimit -a
    - ulimit -s unlimited
    - lscpu | egrep 'Thread|Core|Socket|^CPU\('
    - virtualenv venv
    - source venv/bin/activate
    - pip install numpy matplotlib docopt
    - cmake --version
    - python -V
    - gfortran --version
    - gcc --version
    - g++ --version
    - echo BUILD_NAME=${BUILD_NAME}
    - echo OMP_NUM_THREADS=${OMP_NUM_THREADS}
    - git submodule sync --recursive
    - git submodule update --init --recursive
  script:
    - mkdir -p ${DALTON_TMPDIR}
    - >
        python setup --fc=gfortran --cc=gcc --cxx=g++ --omp --type=debug --check
        -DENABLE_DEC=ON
        -DENABLE_TENSORS=ON
        -DENABLE_RSP=ON
        -DENABLE_XCFUN=ON
        -DENABLE_PCMSOLVER=ON
        -DBUILDNAME="${BUILD_NAME}" ${BUILD_NAME}
    - cd ${BUILD_NAME}
    - echo Build step executing ${BUILD_COMMAND}
    - bash ${CI_PROJECT_DIR}/.scripts/ci_build.sh
    - echo Test step executing ${CTEST_COMMAND}
    - python ${CI_PROJECT_DIR}/.scripts/ci_test.py ${CTEST_COMMAND}
    - python ${CI_PROJECT_DIR}/.scripts/ci_print_failing.py
  artifacts:
    paths:
      - ${BUILD_NAME}/build.log
      - ${BUILD_NAME}/full_ctest_output.dat

hybrid-gcc-int64:
  image: devcafe/ubuntu16.04-gcc5.3.1-openmpi1.10-mkl2017.4.239:latest
  variables:
    BUILD_NAME: "build-hybrid-gcc-int64-mkl_user=${GITLAB_USER_ID}_branch=${CI_COMMIT_REF_NAME}"
    DALTON_NUM_MPI_PROCS: "3"
    OMP_NUM_THREADS: "2"
  before_script:
    - whoami
    - uname -a
    - free -m
    - df -h
    - ulimit -a
    - ulimit -s unlimited
    - lscpu | egrep 'Thread|Core|Socket|^CPU\('
    - virtualenv venv
    - source venv/bin/activate
    - pip install numpy matplotlib docopt
    - cmake --version
    - python -V
    - mpif90 --version
    - mpicc --version
    - mpic++ --version
    - echo BUILD_NAME=${BUILD_NAME}
    - echo DALTON_NUM_MPI_PROCS=${DALTON_NUM_MPI_PROCS}
    - echo OMP_NUM_THREADS=${OMP_NUM_THREADS}
    - git submodule sync --recursive
    - git submodule update --init --recursive
  script:
    - mkdir -p ${DALTON_TMPDIR}
    - >
        python setup --fc=mpif90 --cc=mpicc --cxx=mpic++ --mpi --omp --type=debug --check --int64
        -DENABLE_DEC=ON
        -DENABLE_TENSORS=ON
        -DTENSORS_ENABLE_BRUTE_FORCE_FLUSH=ON
        -DENABLE_RSP=ON
        -DENABLE_XCFUN=ON
        -DENABLE_PCMSOLVER=ON
        -DBUILDNAME="${BUILD_NAME}" ${BUILD_NAME}
    - cd ${BUILD_NAME}
    - echo Build step executing ${BUILD_COMMAND}
    - bash ${CI_PROJECT_DIR}/.scripts/ci_build.sh
    - echo Test step executing ${CTEST_COMMAND}
    - python ${CI_PROJECT_DIR}/.scripts/ci_test.py ${CTEST_COMMAND}
    - python ${CI_PROJECT_DIR}/.scripts/ci_print_failing.py
  artifacts:
    paths:
      - ${BUILD_NAME}/build.log
      - ${BUILD_NAME}/full_ctest_output.dat
