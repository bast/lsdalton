#!/bin/sh
#
# This is the script for generating files for a specific Dalton test job.
#
# For the .check file ksh or bash is preferred, otherwise use sh
# (and hope it is not the old Bourne shell, which will not work)
#
if [ -x /bin/ksh ]; then
   CHECK_SHELL='#!/bin/ksh'
elif [ -x /bin/bash ]; then
   CHECK_SHELL='#!/bin/bash'
else
   CHECK_SHELL='#!/bin/sh'
fi


#######################################################################
#  TEST DESCRIPTION
#######################################################################
cat > IchorLinkFull.info <<'%EOF%'
   IchorLinkFull
   -------------
   Test Purpose:     Test Profile feature of Ichor Integrals
   Molecule:         2 N atoms/UnitTest_genD
%EOF%

#######################################################################
#  MOLECULE INPUT
#######################################################################
cat > IchorLinkFull.mol <<'%EOF%'
BASIS
Turbomole-TZVP
2 NCH molecule
ccsd/cc-pVQZ optimized
AtomTypes=1 Nosymmetry Angstrom
Charge=7.0 Atoms=2
N        0.000000    0.000000    0.115054
N        8.000000    0.000000    0.115054
Charge=6.0 Atoms=2
C        0.000000    0.933817   -0.268460
C        8.000000    0.933817   -0.268460
Charge=1.0 Atoms=2
H        8.808710   -0.466909   -0.268460
H        7.191290   -0.466909   -0.268460
%EOF%

#######################################################################
#  DALTON INPUT
#######################################################################
cat > IchorLinkFull.dal <<'%EOF%'
**GENERAL
.TIME
.NOGCBASIS
**INTEGRALS
.DEBUGICHORLINKFULL
**WAVE FUNCTIONS
.HF
*DENSOPT
.START
H1DIAG
*END OF INPUT
%EOF%

#######################################################################
#  CHECK SCRIPT
#######################################################################
echo $CHECK_SHELL >IchorLinkFull.check
cat >> IchorLinkFull.check <<'%EOF%'
log=$1

if [ `uname` = Linux ]; then
   GREP="egrep -a"
else
   GREP="egrep"
fi

CRIT1=`$GREP "Norm of Ichor4Center: * 1671\.7296166564" $log | wc -l`
TEST[ 1]=`expr  $CRIT1`
CTRL[ 1]=1
ERROR[ 1]=" Profile Run Ichor Integrals NOT CORRECT -"

PASSED=1
for((i=1;i<=1;i++))
do
   if [ ${TEST[i]} -ne ${CTRL[i]} ]; then

      if [ $PASSED -eq 1 ]
      then
         echo -e ${ERROR[i]} > newfile.txt
      else
         echo -e ${ERROR[i]} >> newfile.txt
      fi
      PASSED=0
   fi
done

if [ $PASSED -eq 1 ]
then
   echo TEST ENDED PROPERLY
   exit 0
else
   cat newfile.txt -n
   echo THERE IS A PROBLEM
   exit 1
fi

%EOF%
#######################################################################
