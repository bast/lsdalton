#!/bin/sh
#
# This is the script for generating files for a specific Dalton test job.
#
# For the .check file ksh or bash is preferred, otherwise use sh
# (and hope it is not the old Bourne shell, which will not work)
#
if [ -x /bin/ksh ]; then
   CHECK_SHELL='#!/bin/ksh'
elif [ -x /bin/bash ]; then
   CHECK_SHELL='#!/bin/bash'
else
   CHECK_SHELL='#!/bin/sh'
fi


#######################################################################
#  TEST DESCRIPTION
#######################################################################
cat > linsca_counterpoise.info <<'%EOF%'
   linsca_counterpoise
   -------------
   Molecule:         2 He atoms
   Wave Function:    HF
   Test Purpose:     Check SCF counter poise correction
%EOF%

#######################################################################
#  MOLECULE INPUT
#######################################################################
cat > linsca_counterpoise.mol <<'%EOF%'
BASIS
3-21G
2 He atoms.
Distance in Aangstroms
Atomtypes=2 Charge=0 Angstrom SubSystems
Charge=2   Atoms=1 SubSystem=A
He     0.00000   0.00000   0.00000
Charge=2   Atoms=1 SubSystem=B
He     2.00000   0.00000   0.00000
%EOF%

#######################################################################
#  DALTON INPUT
#######################################################################
cat > linsca_counterpoise.dal <<'%EOF%'
**GENERAL
.INTERACTIONENERGY
.SAMESUBSYSTEMS
.SUBSYSTEMDENSITY
.TIME
**WAVE FUNCTIONS
.DFT
LDA
*DENSOPT
.RH
.DIIS
.NVEC
8
.CONVTHR
5.D-4
.START
H1DIAG
*END OF INPUT
%EOF%

#######################################################################
#  CHECK SCRIPT
#######################################################################
echo $CHECK_SHELL >linsca_counterpoise.check
cat >> linsca_counterpoise.check <<'%EOF%'
log=$1

if [ `uname` = Linux ]; then
   GREP="egrep -a"
else
   GREP="egrep"
fi

# ENERGY test
CRIT1=`$GREP "Final DFT energy: * \-5\.612016" $log | wc -l`
TEST[1]=`expr  $CRIT1`
CTRL[1]=1
ERROR[1]="ENERGY NOT CORRECT -"

# ENERGY test
CRIT1=`$GREP "Energy for subsystem A: * \-2\.806498" $log | wc -l`
CRIT2=`$GREP "Energy for subsystem B: * \-2\.806498" $log | wc -l`
CRIT3=`$GREP "Energy for full System: * \-5\.612016" $log | wc -l`
CRIT4=`$GREP "Interaction Energy    : * 0?\.00098102" $log | wc -l`
TEST[2]=`expr  $CRIT1 \+ $CRIT2 \+ $CRIT3 \+ $CRIT4`
CTRL[2]=4
ERROR[2]="INTERACTION ENERGY -"

# Memory test
CRIT1=`$GREP "Allocated memory \(TOTAL\): * 0 byte" $log | wc -l`
TEST[3]=`expr  $CRIT1`
CTRL[3]=1
ERROR[3]="Memory leak -"

PASSED=1
for i in 1 2 3
do
   if [ ${TEST[i]} -ne ${CTRL[i]} ]; then
      echo ${ERROR[i]}
      PASSED=0
   fi
done

if [ $PASSED -eq 1 ]
then
   echo TEST ENDED PROPERLY
   exit 0
else
   echo THERE IS A PROBLEM
   exit 1
fi

%EOF%
#######################################################################
