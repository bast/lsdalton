# Master branch

The master branch is the main integration branch.

# Release branches

Release branches support the release process
and patches. They are named `release-N.x` where
N is the major version.

# Protected branches

`master` and release branches are protected. We never
push directly to them but only contribute via merge
requests from feature or hotfix branches.
It is also possible to contribute via merge
requests from forks. Whatever people are more comfortable
with.

# Feature branches

Feature branches should always branch off from master.

# Checklist for commits to the master branch or release branches

- Self-contained commits.
- Proper commit message.
- New functionality is documented.
- New functionality is tested.
- Code is sufficiently commented.
- Changelog is updated.
- If the commit fixes an issue, autoclose the issue with the commit message.

# Issue tracking

Create an issue for each bug. Assign it to yourself once you start
working on it.

# Lifetime of branches

All branches should be as short lived as possible.  Naturally a new Ph.D.
student can have long lived branches, but here it is the supervising Post Doc
that need to ensure that the code is modular enough to merge smoothly into
master.  For an experienced developer the life time should be days, weeks, but
preferably not more than that.  Keep the new feature as modular as possible.

# Version numbers

Use sematic versioning: http://semver.org
