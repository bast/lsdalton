#!/bin/sh
#
# This is the script for generating files for a specific Dalton test job.
#
# For the .check file ksh or bash is preferred, otherwise use sh
# (and hope it is not the old Bourne shell, which will not work)
#
if [ -x /bin/ksh ]; then
   CHECK_SHELL='#!/bin/ksh'
elif [ -x /bin/bash ]; then
   CHECK_SHELL='#!/bin/bash'
else
   CHECK_SHELL='#!/bin/sh'
fi


#######################################################################
#  TEST DESCRIPTION
#######################################################################
cat > 00060atoms_0540basfunc_gc_XC.info <<'%EOF%'
   00060atoms_0540basfunc_gc_XC
   -------------
   Molecule:         C60/6-31G
   Wave Function:    B3LYP
   Profile:          Exchange-Correlation Matrix
   CPU Time:         9 min 30 seconds
%EOF%

#######################################################################
#  MOLECULE INPUT
#######################################################################
cat > 00060atoms_0540basfunc_gc_XC.mol <<'%EOF%'
BASIS
6-31G
C60 in B3LYP/6-31G(d,p) optimized geometry
-------------------------------------------
AtomTypes=1 Angstrom
Charge=6.0 Atoms=60
C     3.45100000     0.68500000     0.00000000
C     3.45100000    -0.68500000     0.00000000
C    -3.45100000     0.68500000     0.00000000
C    -3.45100000    -0.68500000     0.00000000
C     0.68500000     0.00000000     3.45100000
C    -0.68500000     0.00000000     3.45100000
C     0.68500000     0.00000000    -3.45100000
C    -0.68500000     0.00000000    -3.45100000
C     0.00000000     3.45100000     0.68500000
C     0.00000000     3.45100000    -0.68500000
C     0.00000000    -3.45100000     0.68500000
C     0.00000000    -3.45100000    -0.68500000
C     3.00400000     1.40900000     1.17200000
C     3.00400000     1.40900000    -1.17200000
C     3.00400000    -1.40900000     1.17200000
C     3.00400000    -1.40900000    -1.17200000
C    -3.00400000     1.40900000     1.17200000
C    -3.00400000     1.40900000    -1.17200000
C    -3.00400000    -1.40900000     1.17200000
C    -3.00400000    -1.40900000    -1.17200000
C     1.40900000     1.17200000     3.00400000
C     1.40900000    -1.17200000     3.00400000
C    -1.40900000     1.17200000     3.00400000
C    -1.40900000    -1.17200000     3.00400000
C     1.40900000     1.17200000    -3.00400000
C     1.40900000    -1.17200000    -3.00400000
C    -1.40900000     1.17200000    -3.00400000
C    -1.40900000    -1.17200000    -3.00400000
C     1.17200000     3.00400000     1.40900000
C    -1.17200000     3.00400000     1.40900000
C     1.17200000     3.00400000    -1.40900000
C    -1.17200000     3.00400000    -1.40900000
C     1.17200000    -3.00400000     1.40900000
C    -1.17200000    -3.00400000     1.40900000
C     1.17200000    -3.00400000    -1.40900000
C    -1.17200000    -3.00400000    -1.40900000
C     2.58100000     0.72400000     2.28000000
C     2.58100000     0.72400000    -2.28000000
C     2.58100000    -0.72400000     2.28000000
C     2.58100000    -0.72400000    -2.28000000
C    -2.58100000     0.72400000     2.28000000
C    -2.58100000     0.72400000    -2.28000000
C    -2.58100000    -0.72400000     2.28000000
C    -2.58100000    -0.72400000    -2.28000000
C     0.72400000     2.28000000     2.58100000
C     0.72400000    -2.28000000     2.58100000
C    -0.72400000     2.28000000     2.58100000
C    -0.72400000    -2.28000000     2.58100000
C     0.72400000     2.28000000    -2.58100000
C     0.72400000    -2.28000000    -2.58100000
C    -0.72400000     2.28000000    -2.58100000
C    -0.72400000    -2.28000000    -2.58100000
C     2.28000000     2.58100000     0.72400000
C    -2.28000000     2.58100000     0.72400000
C     2.28000000     2.58100000    -0.72400000
C    -2.28000000     2.58100000    -0.72400000
C     2.28000000    -2.58100000     0.72400000
C    -2.28000000    -2.58100000     0.72400000
C     2.28000000    -2.58100000    -0.72400000
C    -2.28000000    -2.58100000    -0.72400000
%EOF%

#######################################################################
#  DALTON INPUT
#######################################################################
cat > 00060atoms_0540basfunc_gc_XC.dal <<'%EOF%'
**PROFILE
.XC
**WAVE FUNCTIONS
.DFT
B3LYP
*DENSOPT
.START
H1DIAG
.GCBASIS
*DFT INPUT
.GRID4
.GRID TYPE
BLOCK
*END OF INPUT
%EOF%

#######################################################################
#  CHECK SCRIPT
#######################################################################
echo $CHECK_SHELL >00060atoms_0540basfunc_gc_XC.check
cat >> 00060atoms_0540basfunc_gc_XC.check <<'%EOF%'
log=$1

if [ `uname` = Linux ]; then
   GREP="egrep -a"
else
   GREP="egrep"
fi
CRIT1=`$GREP "Exchange Correlation energy = * \-278\.45526701" $log | wc -l`
TEST[1]=`expr  $CRIT1`
CTRL[1]=2
ERROR[1]="ENERGY NOT CORRECT -"

PASSED=1
for i in 1
do
   if [ ${TEST[i]} -ne ${CTRL[i]} ]; then
      echo ${ERROR[i]}
      PASSED=0
   fi
done

if [ $PASSED -eq 1 ]
then
   echo PROF ENDED PROPERLY
   exit 0
else
   echo THERE IS A PROBLEM
   exit 1
fi

%EOF%
#######################################################################
